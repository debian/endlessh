#!/usr/bin/env bash
set -euo pipefail

KEYS=(
  0x5EEB8C8D5069C4E9B94AA852AFD1503A8C8FF42A # Christopher Wellons <wellons@nullprogram.com>
)

if [ $# -gt 0 ]; then
	exec gpg "$@" "${KEYS[@]}"
else
	exec gpg --export --export-options export-minimal -a --yes \
		-o "$(dirname "$0")/signing-key.asc" "${KEYS[@]}"
fi
